from pythonseleniumproject.Locators import browser as op
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time


op.driver.get("http://the-internet.herokuapp.com/tables")

fn = "John"

xpath ="//table[@id='table1']/tbody/tr/td[2][contains(text(),'NAME')]//following::td[3]".replace("NAME",fn)

web = op.driver.find_element_by_xpath(xpath).text
print(web)


time.sleep(3)
op.driver.close()