from pythonseleniumproject.Locators import browser as op
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time


op.driver.get("https://www.redbus.in/")

op.driver.find_element_by_css_selector("input#src").send_keys("Bang")

time.sleep(3)
pickup = op.driver.find_elements_by_css_selector("ul.autoFill>li")
time.sleep(3)
print("Total no of pickup points are ",len(pickup))

for li in pickup:
    if li.text == "Electronic City, Bangalore":
        li.click()
        break
    # print(li.text)

time.sleep(3)
op.driver.close()