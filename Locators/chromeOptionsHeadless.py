from selenium.webdriver import Chrome,ChromeOptions
from selenium.webdriver.common.keys import Keys

path = "../driver/chromedriver"
ops =  ChromeOptions()
ops.add_argument("--disable-notifications")
ops.add_argument("-incognito")
ops.add_argument("--headless")
driver = Chrome(executable_path=path,options=ops)

driver.get("https://www.google.com/")

driver.find_element_by_partial_link_text("Imag").click()
driver.find_element_by_name('q').send_keys("virginia",Keys.ENTER)
print("page title",driver.title)

driver.quit()
