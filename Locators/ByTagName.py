from pythonseleniumproject.Locators import browser as op
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

op.driver.get("https://www.google.com/")
links=op.driver.find_elements_by_tag_name("a");
print("No of links ",len(links))

for i in links:
    print(i.text," ---> ",i.get_attribute("href"))

time.sleep(2)
op.driver.quit()

