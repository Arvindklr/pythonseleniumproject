from pythonseleniumproject.Locators import browser as op
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

op.driver.get("https://www.google.com/")
links=op.driver.find_elements_by_tag_name("a");
print("No of links ",len(links))

empty=[]

for i in links:
    # print(i.text)
    val = i.text
    empty.append(str(val))

print("Unsorted value: ", empty)
empty.sort(reverse=True)
print("Sorted value: ", empty)

time.sleep(2)
op.driver.quit()

