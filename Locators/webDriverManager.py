from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

# positional param
def launchBrowser(browser="chrome"):
    if browser is "chrome":
        driver = webdriver.Chrome(ChromeDriverManager().install())
    elif browser is "firefox":
        driver = webdriver.Firefox(GeckoDriverManager().install())
    else:
        driver = webdriver.Chrome(ChromeDriverManager().install())

    driver.get("https://www.amazon.com")


b =  launchBrowser("firefox")