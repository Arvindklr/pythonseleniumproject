from selenium.webdriver import Chrome,ChromeOptions

path = "../driver/chromedriver"
ops =  ChromeOptions()
ops.add_argument("--disable-notifications")
ops.add_argument("-incognito")
driver = Chrome(executable_path=path,options=ops)

driver.get("https://www.icicibank.com/")

driver.find_element_by_class_name("pl-login-ornage-box").click()